const VSocket = {
    install(Vue, {options, store}) {
        console.log("VSocket installed");

        if (!store) {
            console.warn("VSocket needs Vuex to run!");
            return;
        }

        // Require vue-session!
        if (Vue.prototype.$session == undefined) {
            console.warn("VSocket needs Vue Session to run!");
            return;
        }

        let session = Vue.prototype.$session;

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        let $VSocket = {
            conn                : null,
            callbackFunctions   : {},
            registeredFunctions : {},
            isReady             : false,
            connectionTimeout   : 10000, /* 10 seconds */
            ready(callback) {
                let self = this;

                if (typeof callback != "function"){
                    console.warn("You must pass a function!");
                    return;
                }
                
                var failAttempt = 0;

                var watchFrequency = 200;

                let interval = setInterval(function(){
                    failAttempt++;
                    if (failAttempt > self.connectionTimeout / watchFrequency) {
                        clearInterval(interval);
                        console.warn("Failed to connect to websocket");
                    }
                    if (self.isReady){
                        callback(self);
                        clearInterval(interval);
                    }
                    
                }, watchFrequency);         
            },
            register(name, f){
                if (typeof f == 'function')
                    this.registeredFunctions[name] = f;
            },
            emit(action, payload = null, callback) {
                if (this.conn && this.ready && store.state.clientData){
                    console.log("Emitting " + action);

                    let clientId    = store.state.clientData.clientId;
                    let fd          = store.state.clientData.fd;

                    let sendData = {
                        action,
                        payload : {
                            clientId,
                            fd
                        }
                    }

                    // User skip payload parameters
                    if (typeof payload == 'function'){
                        this.callbackFunctions[action] = payload;
                    }
                    else if (typeof callback == 'function'){
                        this.callbackFunctions[action] = callback;
                    }
                    
                    if (typeof payload == 'object'){
                        Object.assign(sendData.payload, payload);
                    }

                    this.conn.send(JSON.stringify(sendData));
                }
            },
            init(){
         

                let clientId    = session.get("clientData") == undefined ? null : session.get("clientData").clientId;

                var wsServer;

                if (clientId){
                    wsServer    = options.url + ':' + options.port + '?clientId=' + clientId;
                }
                else {
                    wsServer    = options.url + ':' + options.port;
                }
                

                let conn        = this.conn = new WebSocket(wsServer); 

                let vm          = this;

                conn.onopen = function (evt) { 
                    console.log("Connected to Deception server.");
                };

                conn.onmessage = function (evt) {
                    // console.log(evt.data);
                    if (IsJsonString(evt.data)){
                        let parsedResponse = JSON.parse(evt.data);

                        if (!parsedResponse) return;

                        let clientData = parsedResponse.clientData;

                        if (clientData){

                            session.set("clientData", clientData);

                            store.commit("setClientData", clientData);

                            if (!vm.isReady) vm.isReady = true;
                        }

                        if (parsedResponse.submittedFunc) {

                            // This is the function's name sent from client to server.
                            let callbackFunc = parsedResponse.submittedFunc;

                            // Check if client has callback. The callback is saved from emit function as second or third param        
                            if (typeof vm.callbackFunctions[callbackFunc] == 'function'){

                                // Run it, pass the server data in parsedResponse
                                vm.callbackFunctions[callbackFunc](parsedResponse.responseData);

                                // Delete saved callback function from array
                                delete vm.callbackFunctions[callbackFunc];
                            }
                        }

                        // Check if this is a broadcast from socket server
                        
                        if (parsedResponse.broadcastFunc && typeof vm.registeredFunctions[parsedResponse.broadcastFunc] == 'function'){
                            vm.registeredFunctions[parsedResponse.broadcastFunc](parsedResponse.broadcastFuncData);
                        }
                    }
                    
                };

                conn.onclose = function (evt) { 
                    console.log("Disconnected"); 
                };

                conn.onerror = function (evt, e) {
                    console.log('Error occured: ' + evt.data);
                };
            }
        }

        Vue.prototype.$VSocket = $VSocket;

        $VSocket.init();
    }
};

export default VSocket;