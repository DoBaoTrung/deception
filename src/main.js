// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VSocket from './plugins/v-socket.js'
import VueSession from 'vue-session'

/**
 * VUE NOTIFY
 * https://github.com/se-panfilov/vue-notifications
 */

import VueNotifications from 'vue-notifications'
import iziToast from 'izitoast'// https://github.com/dolce/iziToast
import '@/assets/css/iziToast.deception.css'

// Store
import store from './store/store'

Vue.use(VueSession, {
  persist: process.env.NODE_ENV !== 'development'
})

let VSocketOptions = process.env.SOCKET_ENV

Vue.use(VSocket, { options: VSocketOptions, store, VueSession })

function toast ({ title, message, type, timeout, cb }) {
  if (type === VueNotifications.types.warn) type = 'warning'
  return iziToast[type]({ title, message, timeout: 8000, theme: 'deception', color: 'deception' })
}

const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)

Vue.config.performance = true

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>',
  created () {
    let token = this.$session.get('accessToken')
    if (token) this.$store.commit('setAccessToken', token)
  }
})
