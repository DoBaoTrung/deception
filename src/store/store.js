import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  	state: {
	    clientData: null,
        accessToken: null, /*Current game's access token.*/
  	},
    mutations: {
        setClientData(state, clientData) {
            state.clientData = clientData;
        },
        setAccessToken(state, token) {
            state.accessToken = token;
        }
    },
    getters: {
    	clientData: (state) => {
    		return state.clientData;
    	}
    }
})