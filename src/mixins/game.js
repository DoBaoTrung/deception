import _            from 'lodash'
import PhaseCommons from './phase-commons'
import Phase1 		from './phase1'
import Message      from './message'

export default {
	data(){
		return {
			yourData: null,
	        otherPlayersData: null,
	        gameData: null,
	    }
	},
	mixins: [PhaseCommons, Phase1, Message],
	watch: {
		'currentPhase'(newPhase, oldPhase){
	        switch (newPhase) {
	            case 'Phase1Murderer':
	                this.murdererPick();
	                break;
	            case 'Phase1FS':
	                this.fsPick();
	                break;
	            case 'Arguing':
	                this.arguing();
	                break;
	            case 'PhaseCardChange':
	                this.phaseCardChange();
	                break;
	            case 'LastArguing':
	                this.lastArguing();
	                break;
                case "MurdererLastChance":
                    this.murdererLastChance();
                    break;
	            case 'EndGame':
	                this.endGame();
	                break;
	        }
	    }
	},
	computed:{
		yourRole(){
            if (this.yourData)
                return this.yourData.role;
        },
        isVoteable(){
        	if (this.yourData.isVoteable != true) return false;

        	if (this.yourRole.codename == 'FS') return false;

        	if (this.currentPhase == 'Arguing' || this.currentPhase == 'LastArguing') return true;

        	return false;
        },
        currentPhase(){
            if (this.gameData)
                return this.gameData.currentPhase;
        },
        // Prepare token and room Id
        sendData(){
            if (this.$store.state.clientData){
                let sendData = {
                    roomId: this.$route.params.id, 
                    clientId: this.$store.state.clientData.clientId,
                }

                if (this.$store.state.accessToken){
                    sendData.accessToken = this.$store.state.accessToken;
                }
                return sendData;
            }
        },
        allPlayersData(){
            let yourData                = {};
            yourData[this.yourData.id]  = this.yourData;
            let otherPlayersData        = this.otherPlayersData;
            return {...yourData, ...otherPlayersData};
        },
	},
	methods: {
		getYourData(){
            let vm = this;
            vm.$VSocket.emit("getPlayerData", vm.sendData, function(yourData){
                vm.yourData = yourData;
            });
        },
        getOtherPlayersData(){
            let vm = this;
            vm.$VSocket.emit("getOtherPlayersData", vm.sendData, function(otherPlayersData){
                vm.otherPlayersData = otherPlayersData;
            });
        },
        getAccessToken(){
            let vm = this;

            this.$VSocket.emit("getAccessToken", vm.sendData, function(token){
                // Save access token to session and store.

                vm.$session.set("accessToken", token);
                vm.$store.commit("setAccessToken", token);

                vm.refreshAllGameData();
            });
        },
        getGameData(){
            let vm = this;
            vm.$VSocket.emit("getGameData", vm.sendData, function(gameData){
                vm.gameData = gameData;
            });
        },
        refreshGameData(){
            this.getGameData();
        },
        refreshPlayersData(){
            this.getYourData();
            this.getOtherPlayersData();
        },
        refreshAllGameData(){
            this.getYourData();
            this.getOtherPlayersData();
            this.getGameData();
        },
        showMessage(serverData){
            this.notify({message: serverData.message});
        }
	},
	mounted(){
        let vm = this;

        vm.$VSocket.ready(function(socket){

            socket.register("getAccessToken", vm.getAccessToken);

            socket.register("showMessage", vm.showMessage);

            socket.register("getPingedCard", vm.getPingedCard);

            socket.register("refreshGameData", vm.refreshGameData);

            socket.register("refreshPlayersData", vm.refreshPlayersData);

            socket.register("refreshAllGameData", vm.refreshAllGameData);

        });
    }
}