export default {
	notifications: {
      	notify: { // You can have any name you want instead of 'showLoginError'
	        title: 'Message',
	        message: '.',
	        type: 'success' // You also can use 'VueNotifications.types.error' instead of 'error'
      	},
    }
}