import PhaseCommons from './phase-commons'

export default {
    mixins: [PhaseCommons],
    methods: {
        showMurderConfirmBtn() {
            this.$set(this.misc, 'isMurdererPicking', true);
        },
        clearMurderConfirmBtn(){
            this.$set(this.misc, 'isMurdererPicking', false);
        },
        murdererPick() {
            if (this.yourRole && this.yourRole.codename == 'M') {
                this.yourCardClasses = "selectable";
                this.setPhaseText("Pick your weapons and evidence.");
                this.showMurderConfirmBtn();
            } else this.setPhaseText("Murderer is picking...");
        },
        murdererPickConfirm(){
            
            if (this.chosenCardCount() < 2){
                alert("You must choose both weapon and evidence cards!");
                return;
            }

            let sendData            = this.sendData;
            sendData.pickedCards    = this.selectedCardIDs;
            
            console.log(sendData);

            this.$VSocket.emit("murdererPickConfirm", sendData);
            this.clearAllPickedCards();
        },
        fsPick(){
            this.setPhaseText("Forensic Scientist is choosing...");
            this.clearMurderConfirmBtn();
            this.$set(this.misc, 'isHintCardsShown', true);
            this.$set(this.misc, 'isFSPicking', true);
        },
    }
}