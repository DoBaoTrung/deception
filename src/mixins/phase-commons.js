const TYPE_WEAPON = 0;
const TYPE_EVIDENCE = 1;

let oldMessage = '';

import _            from 'lodash'

export default {
    data() {
        return {
            selectedCardIDs: [],
            selectedCardDestination: null,
            misc: {},
        }
    },
    computed: {
        isNextPhaseBtnVisible(){
            if (this.currentPhase == "Arguing"  && this.yourRole && this.yourRole.codename == 'FS') return true;
            return false;
        },
        isHintCardReplaceable(){
            if (this.currentPhase == 'Phase1Murderer' || this.currentPhase == "EndGame"){
                return false
            }
            return true
        },
    },
    methods: {
        setPhaseText(content, classes="notice"){
            this.gameMessage = [{
                content,
                classes,
            }]
        },
        addPhaseText(content, classes="notice"){
            this.gameMessage.push(
                {
                    content,
                    classes,
                }
            );
        },
        chosenCardCount(){
            let pickedCards = this.selectedCardIDs;

            let pickedCardCount = 0;

            _.forEach(pickedCards, function(card){
                if (card) pickedCardCount++;
            })

            return pickedCardCount;
        },
        /**
         * @param  {[type]} card [description]
         * @param  {[type]} type   [Evidence or weapon card]
         */
        handleCardClick(clientId, card, type) {

            if (this.currentPhase == 'Phase1Murderer' && !card.classes && (this.yourRole.codename == "A" || this.yourRole.codename == "M")){
                let sendData = this.sendData;
                sendData.pingedCard = card.cardId;
                this.$VSocket.emit("pingCard", sendData);
            }

            if (this.chosenCardCount() == 0){
                this.selectedCardDestination = null;
            }

            if (this.selectedCardDestination && this.selectedCardDestination != clientId){
                this.clearAllPickedCards();
            }

            let cards   = [];
            let cardId  = card.cardId;
            let vm      = this;

            var cardKeyName;

            if (type == TYPE_WEAPON){
                cardKeyName = 'weaponCards'; 
            }
            else {
                cardKeyName = 'evidenceCards';
            }

            cards = vm.allPlayersData[clientId][cardKeyName];

            _.forEach(cards, function(card) {
                vm.$set(card, 'classes', '');
            })
            
             // Remove selected card.
            if (_.indexOf(vm.selectedCardIDs, cardId) !== -1){
                let deleteCardIndexes   = _.invert(vm.selectedCardIDs);
                let deleteCardIndex     = deleteCardIndexes[cardId];
                vm.$delete(vm.selectedCardIDs, deleteCardIndex);
                return;
            }

            vm.selectedCardIDs[type] = cardId;

            vm.$set(card, 'classes', 'selected');

            vm.selectedCardDestination = clientId;
        },
        arguing(){
            this.setPhaseText("It's arguing time!");
        },
        lastArguing(){
            this.setPhaseText("This is the last round. Choose wisely..");
        },
        toggleConvict(){
            this.$set(this.misc, 'isConvicting', !this.misc.isConvicting);

            if (this.misc.isConvicting){
                oldMessage = this.gameMessage;
                this.setPhaseText("Select murderer's weapon and evidence!");
            }
            else {
                this.gameMessage = oldMessage;
                this.clearAllPickedCards();
            }
        },
        convict(){
            if (this.chosenCardCount() < 2){
                alert("You must choose both weapon and evidence cards.");
                return;
            }
            let sendData = this.sendData;
            sendData.weaponId = this.selectedCardIDs[0];
            sendData.evidenceId = this.selectedCardIDs[1];
            this.$VSocket.emit("convict", sendData);
            this.toggleConvict();
        },
        phaseCardChange(){
            this.setPhaseText("Forensic Scientist is replacing hint card..");
            this.$set(this.misc, 'isCardChanging', true);
        },
        changeHintCard(replaceCardIndex){
            console.log(replaceCardIndex);
            if (this.misc.isCardChanging && replaceCardIndex && this.yourRole.codename == 'FS'){

                if (replaceCardIndex == 0 || replaceCardIndex == 1) return;

                let sendData                = this.sendData;
                sendData.replaceCardIndex   = replaceCardIndex;
                this.$VSocket.emit("changeHintCard", sendData);
                this.$set(this.misc, 'isCardChanging', false);
            }
        },
        hintCardClick(cardIndex, slotIndex){
            console.log(cardIndex);
            console.log(slotIndex);
            if (this.isHintCardReplaceable && this.yourRole.codename == 'FS' && cardIndex != undefined && slotIndex != undefined){
  
                let sendData            = this.sendData;
                sendData.cardIndex      = cardIndex;
                sendData.slotIndex      = slotIndex;

                this.$VSocket.emit("selectHintCard", sendData);
            }
        },
        murdererLastChance(){
            if (this.yourRole.codename == 'M'){
                this.setPhaseText('Select the witness! Choose wisely!');
            }
            else if (this.yourRole.codename == 'A'){
                this.setPhaseText('Help your partner to find out who the witness is!');
            }
            else this.setPhaseText('Murderer has been revealed.. Waiting for his action..');
        },
        playerNameClick(targetId){
            if (this.currentPhase == "MurdererLastChance"){
                let sendData = this.sendData;
                sendData.targetId = targetId;
                this.$VSocket.emit("murdererWitnessChoose", sendData);
            }
        },
        endGame(){
            let winSide = this.gameData.winSide == 0 ? "Justice" : "Criminal";
            this.setPhaseText("The game has ended. " + winSide + " wins.");
        },
        nextPhase(){
            if (confirm("Are you sure?"))
                this.$VSocket.emit("nextPhase", this.sendData);
        },
        clearAllPickedCards(){
            let vm = this;
            _.forEach(this.allPlayersData, function(player){
                _.forEach(player.evidenceCards, function(card){
                    vm.$set(card, 'classes', null);
                })
                _.forEach(player.weaponCards, function(card){
                    vm.$set(card, 'classes', null);
                })
            })
            vm.selectedCardIDs = [];
        },
        getPingedCard(serverResponse){
            let pingedCard      = serverResponse.pingedCard;
            let vm = this;
            _.forEach(this.allPlayersData, function(player){
                _.forEach(player.evidenceCards, function(card){
                    
                    if (card.cardId == pingedCard){
                        vm.$set(card, 'isPinged', true);
                        setTimeout(function() {
                            vm.$set(card, 'isPinged', false);
                        }, 5000);
                    }       
                })
                _.forEach(player.weaponCards, function(card){
                    if (card.cardId == pingedCard){
                        vm.$set(card, 'isPinged', true);
                        setTimeout(function() {
                            vm.$set(card, 'isPinged', false);
                        }, 5000);
                    }
                })
            })
        },
    }
}