import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home'
import Game from '@/pages/game'
import P404 from '@/pages/404'

Vue.use(Router)

export default new Router({
	mode: 'history',
  	routes: [
	    {
	      	path: '/',
	      	name: 'Home',
	      	component: Home
	    },
	    {
	    	path: '/room/:id',
	    	name: 'Game',
	    	component: Game
	    },
	    { path: "*", component: P404 }
  	]
})
