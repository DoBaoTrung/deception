"use strict";
const merge = require("webpack-merge");
const prodEnv = require("./prod.env");

const SOCKET_ENV = {
    url: '"ws://127.0.0.1"',
    port: '"9502"',
};

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    SOCKET_ENV,
});
